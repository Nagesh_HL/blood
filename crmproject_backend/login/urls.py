from django.conf.urls import url

from .views import register, login, reason, getlistdonors

urlpatterns =[
    url(r'^submitdata',register),
    url(r'^login', login),
    url(r'^reason', reason),
    url(r'^getlistdonors', getlistdonors),
]