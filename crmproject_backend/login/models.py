# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class donor(models.Model):
    firstname = models.CharField(max_length=255)
    lastname = models.CharField(max_length=255)
    bloodgroup = models.CharField(max_length=255)
    contactperson = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    mobile = models.BigIntegerField()
    address = models.CharField(max_length=255)
    state = models.CharField(max_length=255)
    employees = models.CharField(max_length=255)

class Donorslist(models.Model):
    reason = models.CharField(max_length=255)
    mililitre = models.CharField(max_length=255)
    mobile = models.CharField(max_length=255)