angular.module('login', ['ui.bootstrap','ui.router','ngAnimate']);

angular.module('login').config(function($stateProvider) {

    /* Add New States Above */

    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'login/partial/login/login.html',
        controller: 'LoginCtrl'
    });
    $stateProvider.state('signup', {
        url: '/signup',
        templateUrl: 'login/partial/login/signup.html',
        controller: 'LoginCtrl'
    });
    $stateProvider.state('logindashboard', {
        url: '/dashboard',
        templateUrl: 'login/partial/login/dashboard.html',
        controller: 'LoginCtrl'
    });
    $stateProvider.state('admin', {
        url: '/admin',
        templateUrl: 'login/partial/login/admin.html',
        controller: 'LoginCtrl'
    });
});

