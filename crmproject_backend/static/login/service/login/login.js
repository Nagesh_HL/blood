angular.module('login').service('login',function($http) {


 this.signupservice = function(data){
    	return $http({
    		'method':'POST',
    		'url':'/register',
    		'data':data
    	})
    }




this.serviceforregister = function(data){
    	return $http({
    		'method':'POST',
    		'url':'/firststep',
    		'data':data
    	})
    }


this.firststepservice = function(data){
    	return $http({
    		'method':'POST',
    		'url':'/finalregister',
    		'data':data
    	})
    }


   
this.submitserv = function(data){
        return $http({
            'method':'POST',
            'url':'/submitdata',
            'data':data
        })
    }  

this.loginbuttonserv = function(data){
        return $http({
            'method':'POST',
            'url':'/login',
            'data':data
        })
    }

this.donatebloodserv = function(data){
        return $http({
            'method':'POST',
            'url':'/reason',
            'data':data
        })
    }

this.getdonatelistserv = function(data){
        return $http({
            'method':'POST',
            'url':'/getlistdonors',
            'data':data
        })
    }

});